package eu.proszowski.infra;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.toolbox.Volley;

public class RequestQueue {
    private static RequestQueue instance;
    private com.android.volley.RequestQueue requestQueue;
    private static Context ctx;

    private RequestQueue(Context context) {
        ctx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized RequestQueue getInstance(){
        return instance;
    }

    public static synchronized RequestQueue getInstance(Context context) {
        if (instance == null) {
            instance = new RequestQueue(context);
        }
        return instance;
    }

    public com.android.volley.RequestQueue getRequestQueue() {
        if(requestQueue == null){
            return Volley.newRequestQueue(ctx.getApplicationContext());
        }else {
            return requestQueue;
        }
    }

    public <T> void addRequestToQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

}
