package eu.proszowski.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import eu.proszowski.R;
import eu.proszowski.backend.EventBus;
import eu.proszowski.backend.EventName;
import eu.proszowski.backend.LocationProvider;
import eu.proszowski.backend.Scheduler;
import eu.proszowski.backend.TimeProvider;
import eu.proszowski.backend.UnsubscribeAction;

public class TimeAndLocationFragment extends DaggerFragment {

    @BindView(R.id.currentDate)
    TextView currentDate;

    @BindView(R.id.currentTime)
    TextView currentTime;

    @BindView(R.id.currentLocation)
    TextView currentLocation;

    @BindView(R.id.cityName)
    TextView cityName;

    @Inject
    TimeProvider timeProvider;

    @Inject
    LocationProvider locationProvider;

    @Inject
    Scheduler scheduler;

    @Inject
    EventBus eventBus;

    private UnsubscribeAction unsubscribeAction;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        unsubscribeAction.run();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_time_and_location, container, false);
        ButterKnife.bind(this, view);
        currentLocation.setText(String.format("%.2f, %.2f", locationProvider.getLatitude(),
                locationProvider.getLongtitude()));
        unsubscribeAction = eventBus.subscribe(EventName.YAHOO_DATA_FETCHED,
                () -> cityName.setText(locationProvider.getCityName()));
        scheduler.schedule(this::updateTime, 1);
        return view;
    }

    public void updateTime(){
        if(currentTime == null){
            return;
        }
        currentTime.post(() -> currentTime.setText(timeProvider.getCurrentTimeAsString()));
        currentDate.post(() -> currentDate.setText(timeProvider.getCurrentDateAsString()));
    }
}
