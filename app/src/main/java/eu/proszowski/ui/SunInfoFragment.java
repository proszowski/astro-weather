package eu.proszowski.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
import eu.proszowski.R;
import eu.proszowski.backend.EventBus;
import eu.proszowski.backend.EventName;
import eu.proszowski.backend.Scheduler;
import eu.proszowski.backend.SunInfoProvider;
import lombok.extern.java.Log;

@Log
public class SunInfoFragment extends DaggerFragment {

    @BindView(R.id.sunInfoTitle)
    TextView sunInfoTitle;

    @BindView(R.id.sunriseTime)
    TextView sunriseTime;

    @BindView(R.id.sunriseAzimuth)
    TextView sunriseAzimuth;

    @BindView(R.id.sunsetTIme)
    TextView sunsetTime;

    @BindView(R.id.sunsetAzimuth)
    TextView sunsetAzimuth;

    @BindView(R.id.twilghtMorning)
    TextView twilightMorning;

    @BindView(R.id.twilightEvening)
    TextView twilightEvening;

    Unbinder unbinder;

    @Inject
    SunInfoProvider sunInfoProvider;

    @Inject
    EventBus eventBus;

    @Inject
    Scheduler scheduler;

    public SunInfoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        log.info("Hi from SunInfoFragment");
        View view = inflater.inflate(R.layout.fragment_sun_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        updateData();
        eventBus.subscribe(EventName.TIMEZONE_CHANGED, this::updateData);
        scheduler.schedule(this::updateData);
        return view;
    }

    private void updateData() {
        if(sunriseTime == null){
            return;
        }
        sunriseTime.setText("Sunrise: " + sunInfoProvider.getSunrise());
        sunriseAzimuth.setText("Sunrise azimuth: " + sunInfoProvider.getSunriseAzimuth());
        sunsetTime.setText("Sunset: " + sunInfoProvider.getSunset());
        sunsetAzimuth.setText("Sunset azimuth: " + sunInfoProvider.getSunsetAzimuth());
        twilightMorning.setText("Twilight morning: " + sunInfoProvider.getTwilightMorning());
        twilightEvening.setText("Twilight evening: " + sunInfoProvider.getTwilightEvening());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
