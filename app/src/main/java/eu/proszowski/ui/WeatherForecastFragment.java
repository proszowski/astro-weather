package eu.proszowski.ui;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
import eu.proszowski.R;
import eu.proszowski.backend.EventBus;
import eu.proszowski.backend.EventName;
import eu.proszowski.backend.Scheduler;
import eu.proszowski.backend.UnsubscribeAction;
import eu.proszowski.backend.WeatherForecastProvider;
import eu.proszowski.backend.WeatherImageProvider;

public class WeatherForecastFragment extends DaggerFragment {

    @BindViews({R.id.firstDate, R.id.secondDate, R.id.thirdDate, R.id.fourthDate, R.id.fifthDate, R.id.sixthDate})
    List<TextView> dates;

    @BindViews({R.id.firstDescription, R.id.secondDescription, R.id.thirdDescription, R.id.fourthDescription,
            R.id.fifthDescription, R.id.sixthDescription})
    List<TextView> descriptions;

    @BindViews({R.id.firstTemperature, R.id.secondTemperature, R.id.thirdTemperature, R.id.fourthTemperature,
            R.id.fifthTemperature, R.id.sixthTemperature})
    List<TextView> temperatures;

    @BindViews({R.id.firstImageHolder, R.id.secondImageHolder, R.id.thirdImageHolder, R.id.fourthImageHolder,
            R.id.fifthImageHolder, R.id.sixthImageHolder})
    List<RelativeLayout> imageHolders;

    Unbinder unbinder;

    @Inject
    WeatherForecastProvider weatherForecastProvider;

    @Inject
    Scheduler scheduler;

    @Inject
    EventBus eventBus;

    @Inject
    WeatherImageProvider weatherImageProvider;

    private List<UnsubscribeAction> unsubscribeActions = new ArrayList<>();

    @Override
    public void onPause() {
        super.onPause();
        for(UnsubscribeAction unsubscribeAction : unsubscribeActions){
            unsubscribeAction.run();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_forecast, container, false);
        unbinder = ButterKnife.bind(this, view);
        updateData();
        scheduler.schedule(this::updateData);
        unsubscribeActions.add(eventBus.subscribe(EventName.YAHOO_DATA_FETCHED, this::updateData));
        unsubscribeActions.add(eventBus.subscribe(EventName.WEATHER_IMAGE_FETCHED, this::updateData));
        return view;
    }

    private void updateData() {
        if (dates == null || dates.isEmpty()) {
            return;
        }
        List<String> daysStrings = weatherForecastProvider.getDays();
        Map<String, String> descriptionsStrings = weatherForecastProvider.getDescriptions();
        Map<String, String> temperaturesStrings = weatherForecastProvider.getTemperatures();
        for (int i = 0; i < 6; i++) {
            if (daysStrings.isEmpty()) return;
            String day = daysStrings.get(i);
            String description = descriptionsStrings.get(day);
            if (description.length() > 15) {
                description = description.substring(0, 19) + "...";
            }
            String temperature = temperaturesStrings.get(day) + "°C";
            dates.get(i).setText(day);
            temperatures.get(i).setText(temperature);
            descriptions.get(i).setText(description);
            updateWeatherImage(imageHolders.get(i), weatherForecastProvider.getWeatherCodes().get(day));
        }
    }

    private void updateWeatherImage(RelativeLayout imageHolder, String weatherCode) {
        Bitmap bitmap = weatherImageProvider.getImageBitmap(weatherCode);
        if (imageHolder.getChildCount() == 0 && bitmap != null) {
            ImageView imageView = new ImageView(getContext());
            imageHolder.removeAllViews();
            imageView.setImageBitmap(bitmap);
            imageHolder.addView(imageView);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
