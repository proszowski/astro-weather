package eu.proszowski.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
import eu.proszowski.R;
import eu.proszowski.backend.MoonInfoProvider;
import eu.proszowski.backend.Scheduler;
import eu.proszowski.backend.UnsubscribeAction;

public class MoonInfoFragment extends DaggerFragment {

    @BindView(R.id.moonInfoTitle)
    TextView moonInfoTitle;

    @BindView(R.id.moonRise)
    TextView moonRise;

    @BindView(R.id.moonSet)
    TextView moonSet;

    @BindView(R.id.newMoon)
    TextView newMoon;

    @BindView(R.id.fullMoon)
    TextView fullMoon;

    Unbinder unbinder;

    @Inject
    MoonInfoProvider moonInfoProvider;

    @Inject
    Scheduler scheduler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_moon_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        updateData();
        scheduler.schedule(this::updateData);
        return view;
    }

    private void updateData() {
        if(moonRise == null){
            return;
        }
        moonRise.setText("Moon rise: " + moonInfoProvider.getMoonrise());
        moonSet.setText("Moon set: " + moonInfoProvider.getMoonset());
        newMoon.setText("New moon: " + moonInfoProvider.getNewMoon());
        fullMoon.setText("Full moon: " + moonInfoProvider.getFullMoon());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
