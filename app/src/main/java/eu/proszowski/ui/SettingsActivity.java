package eu.proszowski.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import eu.proszowski.R;
import eu.proszowski.backend.CitiesRepository;
import eu.proszowski.backend.EventBus;
import eu.proszowski.backend.InvalidLatitudeException;
import eu.proszowski.backend.InvalidLongitudeException;
import eu.proszowski.backend.SettingsProvider;

import static eu.proszowski.backend.EventName.FETCH_YAHOO_DATA;

public class SettingsActivity extends DaggerAppCompatActivity {

    @Inject
    SettingsProvider settings;

    @BindView(R.id.latitude)
    EditText latitude;

    @BindView(R.id.longitude)
    EditText longitude;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.citiesSpinner)
    Spinner citiesSpinner;

    @Inject
    EventBus eventBus;

    @Inject
    CitiesRepository citiesRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        latitude.setText(String.valueOf(settings.getLatitude()));
        longitude.setText(String.valueOf(settings.getLongitude()));

        ArrayList<String> frequenciesList = new ArrayList<>();
        frequenciesList.add("1");
        frequenciesList.add("3");
        frequenciesList.add("5");
        frequenciesList.add("10");

        ArrayAdapter<String> frequencies = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, frequenciesList);

        spinner.setAdapter(frequencies);

        ArrayAdapter<String> cities = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, new ArrayList<>(citiesRepository.getCities()));

        citiesSpinner.setAdapter(cities);

        citiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String cityName = ((TextView) view).getText().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void updateCities() {
        ArrayAdapter<String> cities = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, new ArrayList<>(citiesRepository.getCities()));
        citiesSpinner.setAdapter(cities);
    }

    public void goBack(View view) {
        Intent main = new Intent(SettingsActivity.this, MainActivity.class);
        startActivity(main);

    }

    public void save(View view) {
        try {
            settings.setLatitude(new BigDecimal(latitude.getText().toString()));
            settings.setLongitude(new BigDecimal(longitude.getText().toString()));
            TextView selectedView = (TextView) spinner.getSelectedView();
            Long interval = Long.parseLong(selectedView.getText().toString()) * 60;
            settings.setRefreshInterval(interval);
            Intent main = new Intent(SettingsActivity.this, MainActivity.class);
            startActivity(main);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Latitude and longitude must be numbers",
                    Toast.LENGTH_LONG).show();
        } catch (InvalidLongitudeException e) {
            Toast.makeText(this, e.getMessage(),
                    Toast.LENGTH_LONG).show();
        } catch (InvalidLatitudeException e) {
            Toast.makeText(this, e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }
    }

    public void fetchYahooData(View view) {
        eventBus.emit(FETCH_YAHOO_DATA);
    }
}
