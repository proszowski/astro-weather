package eu.proszowski.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

class FragmentAdapter extends FragmentStatePagerAdapter {
    private static int NUMBER_OF_ITEMS = 5;
    private final SunInfoFragment sunInfoFragment = new SunInfoFragment();
    private final MoonInfoFragment moonInfoFragment = new MoonInfoFragment();
    private final BasicWeatherFragment basicWeatherFragment = new BasicWeatherFragment();
    private final DetailedWeatherFragment detailedWeatherFragment = new DetailedWeatherFragment();
    private final WeatherForecastFragment weatherForecastFragment = new WeatherForecastFragment();

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return sunInfoFragment;
            case 1:
                return moonInfoFragment;
            case 2:
                return basicWeatherFragment;
            case 3:
                return detailedWeatherFragment;
            case 4:
                return weatherForecastFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUMBER_OF_ITEMS;
    }
}
