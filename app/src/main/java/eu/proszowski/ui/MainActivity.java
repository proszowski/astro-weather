package eu.proszowski.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import eu.proszowski.R;
import eu.proszowski.backend.EventBus;
import eu.proszowski.backend.EventName;
import eu.proszowski.backend.UnsubscribeAction;

public class MainActivity extends DaggerAppCompatActivity {

    @Nullable
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.settings_fab)
    FloatingActionButton settingsFab;

    @Inject
    EventBus eventBus;

    private UnsubscribeAction unsubscribeAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (viewPager != null) {
            viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
        }
        unsubscribeAction = eventBus.subscribe(EventName.YAHOO_RESPONDED_WITH_NULL, this::displayErrorToast);
    }

    private void displayErrorToast() {
        Toast.makeText(this, "Yahoo returned null for provided location",
                Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unsubscribeAction.run();
    }

    public void openSettings(View view) {
        Intent settings = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(settings);
    }
}
