package eu.proszowski.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
import eu.proszowski.R;
import eu.proszowski.backend.BasicWeatherProvider;
import eu.proszowski.backend.EventBus;
import eu.proszowski.backend.EventName;
import eu.proszowski.backend.Scheduler;
import eu.proszowski.backend.UnsubscribeAction;
import eu.proszowski.backend.WeatherImageProvider;
import lombok.extern.java.Log;

@Log
public class BasicWeatherFragment extends DaggerFragment {

    @BindView(R.id.pressure)
    TextView pressure;

    @BindView(R.id.descriptionOfWeather)
    TextView descriptionOfWeather;

    @BindView(R.id.temperature)
    TextView temperature;

    @BindView(R.id.imageHolder)
    RelativeLayout imageHolder;

    Unbinder unbinder;

    @Inject
    BasicWeatherProvider basicWeatherProvider;

    @Inject
    EventBus eventBus;

    @Inject
    Scheduler scheduler;

    @Inject
    WeatherImageProvider weatherImageProvider;

    private List<UnsubscribeAction> unsubscribeActions = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basic_weather, container, false);
        unbinder = ButterKnife.bind(this, view);
        updateData();
        unsubscribeActions.add(eventBus.subscribe(EventName.YAHOO_DATA_FETCHED, this::updateData));
        unsubscribeActions.add(eventBus.subscribe(EventName.WEATHER_IMAGE_FETCHED, this::updateWeatherImage));
        scheduler.schedule(this::updateData);
        return view;
    }

    private void updateData() {
        pressure.setText("Pressure: " + basicWeatherProvider.getPressure() + " mb");
        descriptionOfWeather.setText(basicWeatherProvider.getDescription());
        temperature.setText("Temperature: " + basicWeatherProvider.getTemperature() + "°C");
        updateWeatherImage();
    }

    @Override
    public void onPause() {
        super.onPause();
        for(UnsubscribeAction unsubscribeAction : unsubscribeActions){
            unsubscribeAction.run();
        }
    }

    private void updateWeatherImage() {
        String weatherCode = basicWeatherProvider.getWeatherCode();
        Bitmap bitmap = weatherImageProvider.getImageBitmap(weatherCode);
        if(imageHolder.getChildCount() == 0 && bitmap != null){
            ImageView imageView = new ImageView(getContext());
            imageHolder.removeAllViews();
            imageView.setImageBitmap(bitmap);
            imageHolder.addView(imageView);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
