package eu.proszowski.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
import eu.proszowski.R;
import eu.proszowski.backend.DetailedWeatherProvider;
import eu.proszowski.backend.EventBus;
import eu.proszowski.backend.EventName;
import eu.proszowski.backend.Scheduler;
import eu.proszowski.backend.UnsubscribeAction;

public class DetailedWeatherFragment extends DaggerFragment {

    @BindView(R.id.humidity)
    TextView humidity;

    @BindView(R.id.visibility)
    TextView visibility;

    @BindView(R.id.windSpeed)
    TextView windSpeed;

    @BindView(R.id.windDirection)
    TextView windDirection;

    Unbinder unbinder;

    @Inject
    DetailedWeatherProvider detailedWeatherProvider;

    @Inject
    EventBus eventBus;

    @Inject
    Scheduler scheduler;

    private UnsubscribeAction unsubscribeAction;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detailed_weather, container, false);
        unbinder = ButterKnife.bind(this, view);
        updateData();
        unsubscribeAction = eventBus.subscribe(EventName.YAHOO_DATA_FETCHED, this::updateData);
        scheduler.schedule(this::updateData);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        unsubscribeAction.run();
    }

    private void updateData() {
        humidity.setText("Humidity: " + detailedWeatherProvider.getHumidity() + "%");
        visibility.setText("Visibility: " + detailedWeatherProvider.getVisibility() + "km");
        windSpeed.setText("Wind speed: " + detailedWeatherProvider.getSpeedOfWind() + "km/h");
        windDirection.setText("Wind direction: " + detailedWeatherProvider.getDirectionOfWind());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
