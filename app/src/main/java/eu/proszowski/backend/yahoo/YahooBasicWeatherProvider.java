package eu.proszowski.backend.yahoo;

import eu.proszowski.backend.BasicWeatherProvider;

public class YahooBasicWeatherProvider implements BasicWeatherProvider {

    private YahooClient yahooClient;

    public YahooBasicWeatherProvider(YahooClient yahooClient) {
        this.yahooClient = yahooClient;
    }

    @Override
    public String getPressure() {
        if (yahooClient.getResponse() == null) {
            return "";
        }
        return yahooClient.getResponse().getCurrentObservation().getAtmosphere().getPressure();
    }

    @Override
    public String getDescription() {
        if (yahooClient.getResponse() == null) {
            return "";
        }
        return yahooClient.getResponse().getCurrentObservation().getCondition().getText();
    }

    @Override
    public String getTemperature() {
        if (yahooClient.getResponse() == null) {
            return "";
        }
        return yahooClient.getResponse().getCurrentObservation().getCondition().getTemperature();
    }

    @Override
    public String getWeatherCode() {
        if (yahooClient.getResponse() == null) {
            return "";
        }
        return yahooClient.getResponse().getCurrentObservation().getCondition().getCode();
    }
}
