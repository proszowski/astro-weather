package eu.proszowski.backend.yahoo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.proszowski.backend.WeatherForecastProvider;

public class YahooWeatherForecastProvider implements WeatherForecastProvider {

    private YahooClient yahooClient;

    public YahooWeatherForecastProvider(YahooClient yahooClient) {
        this.yahooClient = yahooClient;
    }

    @Override
    public List<String> getDays() {
        if (yahooClient.getResponse() == null) {
            return Collections.emptyList();
        }
        Forecasts[] forecasts = yahooClient.getResponse().getForecasts();
        List<String> days = new ArrayList<>(forecasts.length);
        for (int i = 1; i < forecasts.length; i++) {
            days.add(forecasts[i].getDay());
        }
        return days;
    }

    @Override
    public Map<String, String> getTemperatures() {
        if (yahooClient.getResponse() == null) {
            return Collections.emptyMap();
        }
        Forecasts[] forecasts = yahooClient.getResponse().getForecasts();
        Map<String, String> temperatures = new HashMap<>();
        for (int i = 1; i < forecasts.length; i++) {
            Double high = Double.valueOf(forecasts[i].getHigh());
            Double low = Double.valueOf(forecasts[i].getLow());
            temperatures.put(forecasts[i].getDay(), String.valueOf((high + low) / 2));
        }
        return temperatures;
    }

    @Override
    public Map<String, String> getDescriptions() {
        if (yahooClient.getResponse() == null) {
            return Collections.emptyMap();
        }
        Forecasts[] forecasts = yahooClient.getResponse().getForecasts();
        Map<String, String> descriptions = new HashMap<>();
        for (int i = 1; i < forecasts.length; i++) {
            descriptions.put(forecasts[i].getDay(), forecasts[i].getText());
        }
        return descriptions;
    }

    @Override
    public Map<String, String> getWeatherCodes() {
        if (yahooClient.getResponse() == null) {
            return Collections.emptyMap();
        }

        Map<String, String> weatherCodes = new HashMap<>();

        for (Forecasts forecasts : yahooClient.getResponse().getForecasts()) {
            weatherCodes.put(forecasts.getDay(), forecasts.getCode());
        }
        return weatherCodes;
    }
}
