package eu.proszowski.backend.yahoo;

import android.util.Pair;

import eu.proszowski.backend.CitiesRepository;
import eu.proszowski.backend.EventBus;
import eu.proszowski.backend.Scheduler;
import eu.proszowski.backend.SettingsProvider;
import eu.proszowski.infra.RequestQueue;
import lombok.extern.java.Log;

import static eu.proszowski.backend.EventName.CITY_ADDED;
import static eu.proszowski.backend.EventName.FETCH_YAHOO_DATA;
import static eu.proszowski.backend.EventName.YAHOO_DATA_FETCHED;
import static eu.proszowski.backend.EventName.YAHOO_RESPONDED_WITH_NULL;

@Log
public class YahooClient {

    private YahooWeatherResponse response;
    private SettingsProvider settingsProvider;
    private EventBus eventBus;
    private CitiesRepository citiesRepository;

    public YahooClient(SettingsProvider settingsProvider, EventBus eventBus, Scheduler scheduler,
                       CitiesRepository citiesRepository) {
        this.eventBus = eventBus;
        this.settingsProvider = settingsProvider;
        this.citiesRepository = citiesRepository;
        eventBus.subscribe(FETCH_YAHOO_DATA, this::fetchData);
        scheduler.schedule(this::fetchData);
    }

    private void fetchData() {
        YahooLatLonRequest yahooLatLonRequest = new YahooLatLonRequest(settingsProvider.getLatitude(),
                settingsProvider.getLongitude(),
                r -> {
                    if (r.getCurrentObservation().getAtmosphere() != null) {
                        response = r;
                        eventBus.emit(YAHOO_DATA_FETCHED);
                        citiesRepository.add(r.getLocation().getCity(),
                                new Pair<>(settingsProvider.getLatitude(),
                                        settingsProvider.getLongitude()));
                        eventBus.emit(CITY_ADDED);
                    } else {
                        eventBus.emit(YAHOO_RESPONDED_WITH_NULL);
                    }
                },
                e -> log.info(e.toString()));
        RequestQueue.getInstance().addRequestToQueue(yahooLatLonRequest);
    }

    public YahooWeatherResponse getResponse() {
        return response;
    }
}
