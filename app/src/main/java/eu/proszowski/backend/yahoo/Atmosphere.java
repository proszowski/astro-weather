package eu.proszowski.backend.yahoo;


import lombok.Data;

@Data
public class Atmosphere
{
    private String rising;
    private String visibility;
    private String humidity;
    private String pressure;
}
