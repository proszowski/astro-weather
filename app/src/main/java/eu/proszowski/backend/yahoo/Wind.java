package eu.proszowski.backend.yahoo;


import lombok.Data;

@Data
public class Wind
{
    private String chill;
    private String speed;
    private String direction;
}

