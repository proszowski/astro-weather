package eu.proszowski.backend.yahoo;


import lombok.Data;

@Data
public class Astronomy
{
    private String sunrise;
    private String sunset;
}



