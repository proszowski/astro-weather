package eu.proszowski.backend.yahoo;


import lombok.Data;

@Data
public class Condition
{
    private String code;
    private String temperature;
    private String text;
}
