package eu.proszowski.backend.yahoo;

import lombok.Data;

@Data
public class Forecasts {
    private String date;
    private String high;
    private String code;
    private String low;
    private String text;
    private String day;
}


