package eu.proszowski.backend.yahoo;

import java.util.Arrays;
import java.util.List;

import eu.proszowski.backend.DetailedWeatherProvider;
import eu.proszowski.backend.yahoo.YahooClient;

public class YahooDetailedWeatherProvider implements DetailedWeatherProvider {

    private YahooClient yahooClient;

    public YahooDetailedWeatherProvider(YahooClient yahooClient) {
        this.yahooClient = yahooClient;
    }

    @Override
    public String getSpeedOfWind() {
        if (yahooClient.getResponse() == null) {
            return "";
        }
        return yahooClient.getResponse().getCurrentObservation().getWind().getSpeed();
    }

    @Override
    public String getDirectionOfWind() {
        if (yahooClient.getResponse() == null) {
            return "";
        }
        List<String> directions = Arrays.asList("North", "North-East", "East", "South-East", "South", "South" +
                "-West", "West", "North-West");
        Double angle = Double.valueOf(yahooClient.getResponse().getCurrentObservation().getWind().getDirection());
        int index = (int) Math.round(((angle %= 360) < 0 ? angle + 360 : angle) / 45) % 8;
        return directions.get(index);
    }

    @Override
    public String getHumidity() {
        if (yahooClient.getResponse() == null) {
            return "";
        }
        return yahooClient.getResponse().getCurrentObservation().getAtmosphere().getHumidity();
    }

    @Override
    public String getVisibility() {
        if (yahooClient.getResponse() == null) {
            return "";
        }
        return yahooClient.getResponse().getCurrentObservation().getAtmosphere().getVisibility();
    }
}
