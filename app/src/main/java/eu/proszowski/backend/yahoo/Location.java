package eu.proszowski.backend.yahoo;


import lombok.Data;

@Data
public class Location {
    private String country;
    private String city;
    private String woeid;
    private String timezone_id;
    private String region;
    private String lat;
    private String longitude;
}
