package eu.proszowski.backend.yahoo;


import lombok.Data;

@Data
public class CurrentObservation
{
    private Atmosphere atmosphere;
    private Condition condition;
    private Astronomy astronomy;
    private String pubDate;
    private Wind wind;
}


