package eu.proszowski.backend.yahoo;

import lombok.Data;

@Data
public class YahooWeatherResponse {
    private Location location;
    private CurrentObservation currentObservation;
    private Forecasts[] forecasts;
}
