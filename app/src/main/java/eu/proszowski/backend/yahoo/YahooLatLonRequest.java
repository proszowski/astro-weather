package eu.proszowski.backend.yahoo;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class YahooLatLonRequest extends JsonRequest<YahooWeatherResponse> {

    private final String appId = "GCTA2E44";
    private final String CONSUMER_KEY =
            "dj0yJmk9YkJwOXk2TVFRZHhCJmQ9WVdrOVIwTlVRVEpGTkRRbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTIx";
    private final String CONSUMER_SECRET = "32162e2a8ad7c5549266d957c3c449667bd140ca";
    private final String baseUrl = "https://weather-ydn-yql.media.yahoo.com/forecastrss";
    private BigDecimal lat;
    private BigDecimal lon;

    public YahooLatLonRequest(BigDecimal lat, BigDecimal lon, Response.Listener<YahooWeatherResponse> listener,
                              Response.ErrorListener errorListener) {
        super(Method.GET, null, null, listener, errorListener);
        this.lat = lat;
        this.lon = lon;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        OAuthConsumer consumer = new OAuthConsumer(null, CONSUMER_KEY, CONSUMER_SECRET, null);
        consumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.HMAC_SHA1);
        OAuthAccessor accessor = new OAuthAccessor(consumer);
        try {
            OAuthMessage request = accessor.newRequestMessage(OAuthMessage.GET, getUrl(), null);
            String authorization = request.getAuthorizationHeader(null);
            headers.put("Authorization", authorization);
        } catch (OAuthException | IOException | URISyntaxException e) {
            throw new AuthFailureError(e.getMessage());
        }

        headers.put("X-Yahoo-App-Id", appId);
        headers.put("Content-Type", "application/json");
        return headers;
    }

    @Override
    public String getUrl() {
        return String.format(baseUrl + "?lat=%s&lon=%s&format=json&u=c", lat, lon);
    }

    @Override
    protected Response<YahooWeatherResponse> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            YahooWeatherResponse parsedResponse = parseResponse(json);
            return Response.success(
                    parsedResponse,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException | JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    private YahooWeatherResponse parseResponse(String jsonObject) {
        jsonObject = jsonObject
                .replaceAll("long", "longitude")
                .replaceAll("current_observation", "currentObservation");
        return new Gson().fromJson(jsonObject, YahooWeatherResponse.class);
    }
}


