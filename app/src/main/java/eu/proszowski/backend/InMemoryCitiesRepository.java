package eu.proszowski.backend;

import android.util.Pair;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class InMemoryCitiesRepository implements CitiesRepository {
    private Map<String, Pair<BigDecimal, BigDecimal>> cities = new HashMap<>();

    @Override
    public void add(String countryName, Pair<BigDecimal, BigDecimal> coordinates) {
        cities.put(countryName.toLowerCase(), coordinates);
    }

    @Override
    public Set<String> getCities() {
        return cities.keySet();
    }

    @Override
    public BigDecimal getLatitude(String cityName) {
        return cities.get(cityName).first;
    }

    @Override
    public BigDecimal getLongitude(String cityName) {
        return cities.get(cityName).second;
    }

}
