package eu.proszowski.backend;

import com.astrocalculator.AstroCalculator;
import com.astrocalculator.AstroDateTime;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class AstroCalculatorWrapper {
    private TimeProvider timeProvider;
    private LocationProvider locationProvider;
    private AstroCalculator astroCalculator;

    public AstroCalculatorWrapper(TimeProvider timeProvider, LocationProvider locationProvider, EventBus eventBus) {
        this.timeProvider = timeProvider;
        this.locationProvider = locationProvider;
        eventBus.subscribe(EventName.TIMEZONE_CHANGED, () -> {
            this.astroCalculator = null;
        });
    }

    public AstroCalculator getInstance() {

        if (astroCalculator == null) {
            Calendar calendar = timeProvider.getCalendar();
            AstroDateTime astroDateTime = new AstroDateTime(
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH) + 1,
                    calendar.get(Calendar.DATE),
                    calendar.get(Calendar.HOUR),
                    calendar.get(Calendar.MINUTE),
                    calendar.get(Calendar.SECOND),
                    (int) TimeUnit.HOURS.convert(locationProvider.getTimeZone().getRawOffset(),
                            TimeUnit.MILLISECONDS),
                    isDst(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_WEEK))
            );
            AstroCalculator.Location location =
                    new AstroCalculator.Location(locationProvider.getLatitude().doubleValue(),
                    locationProvider.getLongtitude().doubleValue());
            return new AstroCalculator(astroDateTime, location);
        } else {
            return astroCalculator;
        }
    }

    public Boolean isDst(int day, int month, int dow) {
        if (month < 3 || month > 10) return false;
        if (month > 3 && month < 10) return true;

        int previousSunday = day - dow;

        if (month == 3) return previousSunday >= 25;
        return previousSunday < 25;
    }

}
