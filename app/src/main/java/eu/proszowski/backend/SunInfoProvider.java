package eu.proszowski.backend;

public interface SunInfoProvider {
    String getSunrise();
    String getSunriseAzimuth();
    String getSunset();
    String getSunsetAzimuth();
    String getTwilightMorning();
    String getTwilightEvening();
}
