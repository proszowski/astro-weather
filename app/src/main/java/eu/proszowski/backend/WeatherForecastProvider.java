package eu.proszowski.backend;

import java.util.List;
import java.util.Map;

public interface WeatherForecastProvider {
    List<String> getDays();

    Map<String, String> getTemperatures();

    Map<String, String> getDescriptions();

    Map<String, String> getWeatherCodes();
}
