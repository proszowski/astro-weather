package eu.proszowski.backend;

public interface EventBus {
    UnsubscribeAction subscribe(EventName eventName, Runnable runnable);
    void emit(EventName eventName);
}
