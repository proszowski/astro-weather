package eu.proszowski.backend;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class LocalTimeProvider implements TimeProvider {

    private LocationProvider locationProvider;

    public LocalTimeProvider(LocationProvider locationProvider) {
        this.locationProvider = locationProvider;
    }

    @Override
    public String getCurrentTimeAsString() {
        return getFormattedDate("HH:mm:ss");
    }

    @Override
    public String getCurrentDateAsString() {
        return getFormattedDate("dd-MM-yyyy");
    }

    private String getFormattedDate(String format) {
        Calendar calendar = getCalendar();

        DateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    @Override
    public Calendar getCalendar() {
        Calendar calendar = Calendar.getInstance();
        TimeZone fromTimeZone = calendar.getTimeZone();
        TimeZone toTimeZone = locationProvider.getTimeZone();

        calendar.setTimeZone(fromTimeZone);
        calendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset() * -1);
        if (fromTimeZone.inDaylightTime(calendar.getTime())) {
            calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
        }

        calendar.add(Calendar.MILLISECOND, toTimeZone.getRawOffset());
        if (toTimeZone.inDaylightTime(calendar.getTime())) {
            calendar.add(Calendar.MILLISECOND, toTimeZone.getDSTSavings());
        }
        return calendar;
    }

    private Locale getLocale() {
        return Locale.getDefault();
    }
}
