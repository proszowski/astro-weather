package eu.proszowski.backend;

import android.util.Pair;

import java.math.BigDecimal;
import java.util.Set;

public interface CitiesRepository {
    void add(String countryName, Pair<BigDecimal, BigDecimal> coordinates);

    Set<String> getCities();

    BigDecimal getLatitude(String cityName);

    BigDecimal getLongitude(String cityName);
}
