package eu.proszowski.backend;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageRequest;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import eu.proszowski.backend.yahoo.Forecasts;
import eu.proszowski.backend.yahoo.YahooClient;
import eu.proszowski.infra.RequestQueue;
import lombok.extern.java.Log;

@Log
public class YahooWeatherImageProvider implements WeatherImageProvider {

    private static final String URL_TEMPLATE = "http://l.yimg.com/a/i/us/we/52/%s.gif";
    private YahooClient yahooClient;
    private EventBus eventBus;
    private Map<String, Bitmap> images = new HashMap<>();

    public YahooWeatherImageProvider(YahooClient yahooClient, EventBus eventBus) {
        this.yahooClient = yahooClient;
        this.eventBus = eventBus;
        eventBus.subscribe(EventName.YAHOO_DATA_FETCHED, this::fetchImages);
    }

    private void fetchImages() {
        for (String weatherCode : getImagesWeatherCodes()) {
            ImageRequest weatherImageRequest = new ImageRequest(
                    String.format(URL_TEMPLATE, weatherCode),
                    (response) -> {
                        images.put(weatherCode, response);
                        eventBus.emit(EventName.WEATHER_IMAGE_FETCHED);
                    },
                    100,
                    100,
                    ImageView.ScaleType.CENTER,
                    Bitmap.Config.RGB_565,
                    (e) -> log.info(e.toString()));
            RequestQueue.getInstance().addRequestToQueue(weatherImageRequest);
        }
    }

    private List<String> getImagesWeatherCodes() {
        if (yahooClient.getResponse() == null) {
            return Collections.emptyList();
        }
        List<String> weatherCodes = new LinkedList<>();
        String weatherCode = yahooClient.getResponse().getCurrentObservation().getCondition().getCode();
        weatherCodes.add(weatherCode);
        for (Forecasts forecasts : yahooClient.getResponse().getForecasts()) {
            weatherCodes.add(forecasts.getCode());
        }
        return weatherCodes;
    }

    @Override
    public Bitmap getImageBitmap(String weatherCode) {
        return images.get(weatherCode);
    }
}
