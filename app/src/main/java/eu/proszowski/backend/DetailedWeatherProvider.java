package eu.proszowski.backend;

public interface DetailedWeatherProvider {

    String getSpeedOfWind();

    String getDirectionOfWind();

    String getHumidity();

    String getVisibility();
}
