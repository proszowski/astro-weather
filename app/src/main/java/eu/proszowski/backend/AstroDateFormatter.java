package eu.proszowski.backend;

import com.astrocalculator.AstroDateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AstroDateFormatter {

    private LocationProvider locationProvider;

    public AstroDateFormatter(LocationProvider locationProvider) {
        this.locationProvider = locationProvider;
    }

    public String getFormattedDate(AstroDateTime astroDateTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(
                astroDateTime.getYear(),
                astroDateTime.getMonth() - 1,
                astroDateTime.getDay(),
                astroDateTime.getHour(),
                astroDateTime.getMinute(),
                astroDateTime.getSecond()
        );
        calendar.setTimeZone(locationProvider.getTimeZone());
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return dateFormat.format(calendar.getTime());
    }
}
