package eu.proszowski.backend;

import java.math.BigDecimal;
import java.util.TimeZone;

public interface LocationProvider {
    BigDecimal getLongtitude();

    BigDecimal getLatitude();

    TimeZone getTimeZone();

    String getCityName();
}
