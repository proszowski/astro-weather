package eu.proszowski.backend;

public class AstroSunInfoProvider implements SunInfoProvider {

    private AstroCalculatorWrapper astroCalculatorWrapper;
    private AstroDateFormatter astroDateFormatter;

    public AstroSunInfoProvider(AstroCalculatorWrapper astroCalculatorWrapper, AstroDateFormatter astroDateFormatter) {
        this.astroCalculatorWrapper = astroCalculatorWrapper;
        this.astroDateFormatter = astroDateFormatter;
    }

    @Override
    public String getSunrise() {
        return astroDateFormatter.getFormattedDate(astroCalculatorWrapper.getInstance().getSunInfo().getSunrise());
    }

    @Override
    public String getSunriseAzimuth() {
        return String.valueOf(astroCalculatorWrapper.getInstance().getSunInfo().getAzimuthRise());
    }

    @Override
    public String getSunset() {
        return astroDateFormatter.getFormattedDate(astroCalculatorWrapper.getInstance().getSunInfo().getSunset());
    }

    @Override
    public String getSunsetAzimuth() {
        return String.valueOf(astroCalculatorWrapper.getInstance().getSunInfo().getAzimuthSet());
    }

    @Override
    public String getTwilightMorning() {
        return astroDateFormatter.getFormattedDate(astroCalculatorWrapper.getInstance().getSunInfo().getTwilightMorning());
    }

    @Override
    public String getTwilightEvening() {
        return astroDateFormatter.getFormattedDate(astroCalculatorWrapper.getInstance().getSunInfo().getTwilightEvening());
    }
}
