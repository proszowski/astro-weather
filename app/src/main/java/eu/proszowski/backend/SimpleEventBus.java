package eu.proszowski.backend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.java.Log;

@Log
public class SimpleEventBus implements EventBus {

    Map<EventName, List<Runnable>> subscribers = new HashMap<>();

    @Override
    public UnsubscribeAction subscribe(EventName eventName, Runnable runnable) {
        List<Runnable> runnables = subscribers.get(eventName);
        if (runnables == null) {
            runnables = new ArrayList<>();
        }

        runnables.add(runnable);
        subscribers.put(eventName, runnables);

        return () -> unsubscribe(eventName, runnable);
    }

    private void unsubscribe(EventName eventName, Runnable runnable) {
        List<Runnable> runnables = subscribers.get(eventName);
        if (runnables != null) {
            runnables.remove(runnable);
        }
    }

    @Override
    public void emit(EventName eventName) {
        List<Runnable> runnables = subscribers.get(eventName);
        if (runnables != null) {
            for (Runnable runnable : runnables) {
                runnable.run();
            }
        }
    }
}
