package eu.proszowski.backend;

import java.math.BigDecimal;

public interface SettingsProvider {

    BigDecimal getLongitude();

    BigDecimal getLatitude();

    void setLatitude(BigDecimal bigDecimal);

    void setLongitude(BigDecimal bigDecimal);

    Long getRefreshInterval();

    void setRefreshInterval(Long refreshInterval);
}
