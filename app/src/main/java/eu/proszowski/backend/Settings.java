package eu.proszowski.backend;

import java.math.BigDecimal;

import static eu.proszowski.backend.EventName.LOCATION_CHANGED;
import static eu.proszowski.backend.EventName.REFRESH_INTERVAL_CHANGED;

public class Settings implements SettingsProvider {

    private BigDecimal latitude = new BigDecimal("50.85");
    private BigDecimal longitude = new BigDecimal("4.35");
    private Long refreshInterval = 60L;
    private EventBus eventBus;

    public Settings(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public BigDecimal getLongitude() {
        return longitude;
    }

    @Override
    public void setLongitude(BigDecimal longitude) {
        if(longitude.doubleValue() > 180 || longitude.doubleValue() < -180){
            throw new InvalidLongitudeException(longitude);
        }
        this.longitude = longitude;
        eventBus.emit(LOCATION_CHANGED);
    }

    @Override
    public Long getRefreshInterval() {
        return refreshInterval;
    }

    @Override
    public void setRefreshInterval(Long refreshInterval) {
        this.refreshInterval = refreshInterval;
        eventBus.emit(REFRESH_INTERVAL_CHANGED);
    }

    @Override
    public BigDecimal getLatitude() {
        return latitude;
    }

    @Override
    public void setLatitude(BigDecimal latitude) {
        if(latitude.doubleValue() > 90 || latitude.doubleValue() < -90){
            throw new InvalidLatitudeException(latitude);
        }
        this.latitude = latitude;
        eventBus.emit(LOCATION_CHANGED);
    }
}
