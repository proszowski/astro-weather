package eu.proszowski.backend;

import java.math.BigDecimal;

public class InvalidLatitudeException extends RuntimeException{
    public InvalidLatitudeException(BigDecimal latitude) {
        super("Latitude must be in range <-90, 90>");
    }
}
