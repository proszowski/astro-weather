package eu.proszowski.backend;

import java.math.BigDecimal;

public class InvalidLongitudeException extends RuntimeException {
    public InvalidLongitudeException(BigDecimal longitude) {
        super("Longitude must be in range <-180, 180>");
    }
}
