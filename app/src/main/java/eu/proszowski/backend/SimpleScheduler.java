package eu.proszowski.backend;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static eu.proszowski.backend.EventName.REFRESH_INTERVAL_CHANGED;

public class SimpleScheduler implements Scheduler {

    private ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    private List<Runnable> runnables = new ArrayList<>();
    private Long timeIntervalInSeconds;

    public SimpleScheduler(EventBus eventBus, SettingsProvider settingsProvider) {
        this.timeIntervalInSeconds = settingsProvider.getRefreshInterval();
        eventBus.subscribe(REFRESH_INTERVAL_CHANGED, () -> {
            scheduledExecutorService.shutdown();
            scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
            this.timeIntervalInSeconds = settingsProvider.getRefreshInterval();
            for (int i = 0; i < runnables.size(); i++) {
                scheduledExecutorService.scheduleAtFixedRate(runnables.get(i), 0,
                        timeIntervalInSeconds, TimeUnit.SECONDS);
            }
        });
    }

    @Override
    public void schedule(Runnable runnable) {
        runnables.add(runnable);
        scheduledExecutorService.scheduleAtFixedRate(runnable, 0, timeIntervalInSeconds,
                TimeUnit.SECONDS);

    }

    @Override
    public void schedule(Runnable runnable, Integer timeIntervalInSeconds) {
        runnables.add(runnable);
        scheduledExecutorService.scheduleAtFixedRate(runnable, 0, timeIntervalInSeconds,
                TimeUnit.SECONDS);

    }
}
