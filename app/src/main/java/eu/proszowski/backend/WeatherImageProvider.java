package eu.proszowski.backend;

import android.graphics.Bitmap;

public interface WeatherImageProvider {
    Bitmap getImageBitmap(String weatherCode);
}
