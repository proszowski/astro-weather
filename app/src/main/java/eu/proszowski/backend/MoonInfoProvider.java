package eu.proszowski.backend;

public interface MoonInfoProvider {
    String getMoonrise();

    String getMoonset();

    String getNewMoon();

    String getFullMoon();

    String getMoonPhase();

    String getSynopticDay();
}
