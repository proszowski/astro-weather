package eu.proszowski.backend;

public interface Scheduler {
    void schedule(Runnable runnable);

    void schedule(Runnable runnable, Integer timeIntervalInSeconds);
}
