package eu.proszowski.backend;

public interface BasicWeatherProvider {

    String getPressure();

    String getDescription();

    String getTemperature();

    String getWeatherCode();
}
