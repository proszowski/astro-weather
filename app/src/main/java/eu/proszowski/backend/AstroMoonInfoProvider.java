package eu.proszowski.backend;

public class AstroMoonInfoProvider implements MoonInfoProvider {

    AstroCalculatorWrapper astroCalculatorWrapper;
    AstroDateFormatter astroDateFormatter;

    public AstroMoonInfoProvider(AstroCalculatorWrapper astroCalculatorWrapper,
                                  AstroDateFormatter astroDateFormatter) {
        this.astroCalculatorWrapper = astroCalculatorWrapper;
        this.astroDateFormatter = astroDateFormatter;
    }

    @Override
    public String getMoonrise() {
        return astroDateFormatter.getFormattedDate(astroCalculatorWrapper.getInstance().getMoonInfo().getMoonrise());
    }

    @Override
    public String getMoonset() {
        return astroDateFormatter.getFormattedDate(astroCalculatorWrapper.getInstance().getMoonInfo().getMoonset());
    }

    @Override
    public String getNewMoon() {
        return astroDateFormatter.getFormattedDate(astroCalculatorWrapper.getInstance().getMoonInfo().getNextNewMoon());
    }

    @Override
    public String getFullMoon() {
        return astroDateFormatter.getFormattedDate(astroCalculatorWrapper.getInstance().getMoonInfo().getNextFullMoon());
    }

    @Override
    public String getMoonPhase() {
        Double age = astroCalculatorWrapper.getInstance().getMoonInfo().getAge();
        return String.valueOf(age);
    }

    @Override
    public String getSynopticDay() {
        return "synoptic day";
    }
}
