package eu.proszowski.backend;

import java.util.Calendar;
import java.util.Date;

public interface TimeProvider {

    Calendar getCalendar();

    String getCurrentTimeAsString();

    String getCurrentDateAsString();
}
