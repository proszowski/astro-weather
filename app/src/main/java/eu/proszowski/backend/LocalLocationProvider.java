package eu.proszowski.backend;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.TimeZone;

import eu.proszowski.backend.yahoo.YahooClient;
import eu.proszowski.infra.RequestQueue;

import static eu.proszowski.backend.EventName.LOCATION_CHANGED;
import static eu.proszowski.backend.EventName.TIMEZONE_CHANGED;

public class LocalLocationProvider implements LocationProvider {

    private final static String URL_TEMPLATE = "http://api.timezonedb.com/v2" +
            ".1/get-time-zone?key=I8FEKOHFP7UK&format=json&lat" +
            "=%s&lng=%s&by=position";
    private YahooClient yahooClient;
    private SettingsProvider settingsProvider;
    private TimeZone timeZone;
    private EventBus eventBus;
    public LocalLocationProvider(YahooClient yahooClient, SettingsProvider settingsProvider, EventBus eventBus) {
        this.yahooClient = yahooClient;
        this.settingsProvider = settingsProvider;
        this.eventBus = eventBus;
        updateData();
        eventBus.subscribe(LOCATION_CHANGED, this::updateData);
    }

    @Override
    public String getCityName() {
        if (yahooClient.getResponse() == null) {
            return "";
        }
        return yahooClient.getResponse().getLocation().getCity();
    }

    private void updateData() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, String.format(URL_TEMPLATE, getLatitude(),
                getLongtitude()), null, this::handleResponse, System.out::println);
        RequestQueue.getInstance().addRequestToQueue(request);
    }

    private void handleResponse(JSONObject jsonObject) {
        try {
            String timeZoneId = jsonObject.getString("zoneName");
            timeZone = TimeZone.getTimeZone(timeZoneId);
            eventBus.emit(TIMEZONE_CHANGED);
        } catch (JSONException e) {
            timeZone = TimeZone.getDefault();
        }
    }

    @Override
    public BigDecimal getLongtitude() {
        return settingsProvider.getLongitude();
    }

    @Override
    public BigDecimal getLatitude() {
        return settingsProvider.getLatitude();
    }

    @Override
    public TimeZone getTimeZone() {
        if (timeZone != null) {
            return timeZone;
        } else {
            return TimeZone.getDefault();
        }
    }
}
