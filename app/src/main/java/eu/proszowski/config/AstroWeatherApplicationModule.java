package eu.proszowski.config;


import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import eu.proszowski.backend.AstroCalculatorWrapper;
import eu.proszowski.backend.AstroDateFormatter;
import eu.proszowski.backend.AstroMoonInfoProvider;
import eu.proszowski.backend.AstroSunInfoProvider;
import eu.proszowski.backend.BasicWeatherProvider;
import eu.proszowski.backend.CitiesRepository;
import eu.proszowski.backend.DetailedWeatherProvider;
import eu.proszowski.backend.EventBus;
import eu.proszowski.backend.InMemoryCitiesRepository;
import eu.proszowski.backend.LocalLocationProvider;
import eu.proszowski.backend.LocalTimeProvider;
import eu.proszowski.backend.LocationProvider;
import eu.proszowski.backend.MoonInfoProvider;
import eu.proszowski.backend.Scheduler;
import eu.proszowski.backend.Settings;
import eu.proszowski.backend.SettingsProvider;
import eu.proszowski.backend.SimpleEventBus;
import eu.proszowski.backend.SimpleScheduler;
import eu.proszowski.backend.SunInfoProvider;
import eu.proszowski.backend.TimeProvider;
import eu.proszowski.backend.WeatherForecastProvider;
import eu.proszowski.backend.WeatherImageProvider;
import eu.proszowski.backend.YahooWeatherImageProvider;
import eu.proszowski.backend.yahoo.YahooBasicWeatherProvider;
import eu.proszowski.backend.yahoo.YahooClient;
import eu.proszowski.backend.yahoo.YahooDetailedWeatherProvider;
import eu.proszowski.backend.yahoo.YahooWeatherForecastProvider;
import eu.proszowski.ui.BasicWeatherFragment;
import eu.proszowski.ui.DetailedWeatherFragment;
import eu.proszowski.ui.MainActivity;
import eu.proszowski.ui.MoonInfoFragment;
import eu.proszowski.ui.SettingsActivity;
import eu.proszowski.ui.SunInfoFragment;
import eu.proszowski.ui.TimeAndLocationFragment;
import eu.proszowski.ui.WeatherForecastFragment;

@Module
public abstract class AstroWeatherApplicationModule {

    private final static CitiesRepository citiesRepository = new InMemoryCitiesRepository();
    private final static EventBus eventBus = new SimpleEventBus();
    private final static Settings settings = new Settings(eventBus);
    private final static Scheduler scheduler = new SimpleScheduler(eventBus, settings);
    private final static YahooClient yahooClient = new YahooClient(providesSettingsProvider(), eventBus, scheduler,
            citiesRepository);
    private final static LocationProvider locationProvider = new LocalLocationProvider(yahooClient,
            providesSettingsProvider(),
            eventBus);
    private final static AstroDateFormatter astroDateFormatter = new AstroDateFormatter(locationProvider);
    private final static WeatherImageProvider weatherImageProvider = new YahooWeatherImageProvider(yahooClient,
            eventBus);

    @Provides
    static WeatherImageProvider providesWeatherImageProvider() {
        return weatherImageProvider;
    }

    @Provides
    static CitiesRepository providesCityRepository() {
        return citiesRepository;
    }

    @Provides
    static WeatherForecastProvider providesWeatherForecastProvider() {
        return new YahooWeatherForecastProvider(yahooClient);
    }

    @Provides
    static DetailedWeatherProvider providesDetailedWeatherProvider() {
        return new YahooDetailedWeatherProvider(yahooClient);
    }

    @Provides
    static BasicWeatherProvider providesBasicWeatherProvider() {
        return new YahooBasicWeatherProvider(yahooClient);
    }

    @Provides
    static YahooClient providesYahooClient() {
        return yahooClient;
    }

    @Provides
    static AstroDateFormatter providesAstroDateFormatter() {
        return astroDateFormatter;
    }

    @Provides
    static MoonInfoProvider moonInfoProvider() {
        return new AstroMoonInfoProvider(providesAstroCalculator(), providesAstroDateFormatter());
    }

    @Provides
    static Scheduler providesScheduler() {
        return scheduler;
    }

    @Provides
    static EventBus providesEventBus() {
        return eventBus;
    }

    @Provides
    static SettingsProvider providesSettingsProvider() {
        return settings;
    }

    @Provides
    static LocationProvider providesLocationProvider() {
        return locationProvider;
    }

    @Provides
    static TimeProvider providesTimeProvider() {
        return new LocalTimeProvider(providesLocationProvider());
    }

    @Provides
    static AstroCalculatorWrapper providesAstroCalculator() {
        return new AstroCalculatorWrapper(providesTimeProvider(), providesLocationProvider(), eventBus);
    }

    @Provides
    static SunInfoProvider sunInfoProvider() {
        return new AstroSunInfoProvider(providesAstroCalculator(), providesAstroDateFormatter());
    }

    @ContributesAndroidInjector
    abstract MainActivity mainActivityContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract MoonInfoFragment moonInfoFragmentContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract SunInfoFragment sunInfoFragmentContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract TimeAndLocationFragment timeAndLocationFragmentInfoFragmentContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract SettingsActivity settingsActivityContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract BasicWeatherFragment basicWeatherFragmentContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract DetailedWeatherFragment detailedWeatherFragmentContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract WeatherForecastFragment weatherForecastFragmentContributesAndroidInjector();
}

