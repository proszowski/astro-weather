package eu.proszowski.config;

import android.content.Context;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import eu.proszowski.ui.MainActivity;

@Component(modules = {AndroidInjectionModule.class, AstroWeatherApplicationModule.class})
public interface AstroWeatherComponent extends AndroidInjector<AstroWeatherApplication> {
}
